export function articles(state = [], action){
    const { type, articles } = action;

    switch (type){
        case 'ARTICLES_FETCH_DATA_SUCCESS':
            return articles;
        default:
            return state;
    }

}