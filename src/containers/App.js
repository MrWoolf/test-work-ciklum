import React, { Component } from 'react';
import { connect } from 'react-redux';
import { articlesFetchData } from '../actions/articles';
import ArticleBuilder from './ArticleBuilder';
import Navigation from './Navigation';
import { Route, HashRouter } from 'react-router-dom';


const mapStateToProps = state => {
    return {
        articles: state.articles
    };
}
  
const mapDispatchToProps = dispatch => {
    return {
        fetchData: url => dispatch(articlesFetchData(url))
    };
}

class App extends Component {
    componentDidMount(){
        this.props.fetchData('aller-structure-task/test_data.json');
    }

    render(){
        return(
            <div>
                <Navigation/>
                <HashRouter>
                    <Route path='/extended-articles' render={(props)=><ArticleBuilder mode='extended' articles={this.props.articles} {...props}/>}/>
                    <Route path='/base-articles' render={(props)=><ArticleBuilder mode='base' articles={this.props.articles} {...props}/>}/>
                </HashRouter>
            </div>
        );
    }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(App);
