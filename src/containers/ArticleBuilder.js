import React, { Component } from 'react';
import RowEntity from './RowEntity';

class ArticleBuilder extends Component {
    buildArticles = articleEntities => {
        return articleEntities.map(({ type, columns }, index) => {
            return (
                <div key={index} className={type.toLocaleLowerCase()}>
                    {this.getColumnsEntities(columns)}
                </div>
            );
        })
    }
    
    
    getColumnsEntities = columns => {
        return columns.map((entity, index) => {
            return <RowEntity mode={this.props.mode} entity={entity} key={index}/>
        });
    }

    render() {
        return (
            <div className="container">
                {this.props.articles.map(articleEntities => this.buildArticles(articleEntities))}
            </div>
        );
    }
}

export default ArticleBuilder;