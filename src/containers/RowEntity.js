import React, { Component } from 'react';
import EditableTitle from './EditableTitle';

class RowEntity extends Component {
    constructor(props) {
        super(props);
    
        this.entityRef = React.createRef();
    }

    state = {
        isInRemovedMode: false,
        deleteNode: false,
        timeout: 10000
    }

    onDelete = () => {
        this.updateComponentEntity();

        this.timer = setTimeout(()=>{
            this.setState({
                deleteNode: true
            });
        }, this.state.timeout);
    }

    onRestore = () => {
        this.updateComponentEntity();
        clearTimeout(this.timer);
    }

    updateComponentEntity = () => {
        this.setState({
            isInRemovedMode: !this.state.isInRemovedMode,
        });
    }

    renderDefaultView = () => {
        const { imageUrl, title, type, url, width } = this.props.entity;

        if (this.props.mode === 'extended') {
            return (
                <div className={`col-sm-${width} article-entity`}> 
                    <EditableTitle 
                        text={title} 
                        url={url}
                        type={type}
                        mode='extended'
                    />
                    <img
                        className='img-fluid' 
                        src={`${imageUrl}&width=650`} 
                        alt={title}
                    />
                    <div className='button-container'>
                        <button onClick={this.onDelete} type="button" className="btn btn-danger">Delete</button>
                    </div>
                </div>
            );
        } else {
            return (
                <div className='col-sm-12 article-entity'>
                    <EditableTitle 
                        text={title}
                        type={type}
                        mode='base'
                    />
                    <button onClick={this.onDelete} type="button" className="btn btn-danger">Delete</button>
                </div>
            );
        }
    }

    renderRemovedView = () => {
        return (
            <div className={`col-sm-${this.props.mode === 'extended' ? this.props.entity.width : '12'} article-entity`}>
                <span className='restore-title'>This article has been removed, you have {this.state.timeout / 1000} seconds to restore it</span>
                <button onClick={this.onRestore} type="button" className="btn btn-success">Restore</button>
            </div>
        );
    }

    render(){
        const width = `col-sm-${this.props.entity.width}`;

        if (this.state.deleteNode) return <div className={width}></div>;

        return this.state.isInRemovedMode ? this.renderRemovedView() : this.renderDefaultView();
    }
}

export default RowEntity;