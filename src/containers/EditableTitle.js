import React, { Component } from 'react';

class EditableTitle extends Component {
    constructor(props) {
        super(props);
    
        this.inputRef = React.createRef();
    }

    state = {
        isInEditMode: false,
        value: this.props.text
    }

    chengeEditMode = () => {
        this.setState({
            isInEditMode: !this.state.isInEditMode
        });
    }

    updateComponentValue = () => {
        this.setState({
            isInEditMode: false,
            value: this.inputRef.current.value
        });
    }

    renderEditView = () => {
        return (
            <div>
                <input
                    type='text'
                    className='input-article'
                    defaultValue={this.state.value}
                    ref={this.inputRef}
                />
                <button onClick={this.updateComponentValue} type="button" className="btn btn-success">Ok</button>
                <button onClick={this.chengeEditMode} type="button" className="btn btn-danger">Cancel</button>
            </div>
        );
    }

    renderDefaultView = () => {
        return this.props.mode === 'extended' ? 
            <h5>
                <a className='article-title' href={this.props.url}>{this.state.value}</a> 
                <button onClick={this.chengeEditMode} type="button" className="btn btn-secondary">Edit</button>
            </h5>
            :
            <span className='article-title-wrapper'>
                <span className='article-title'>{this.state.value}</span>
                <button onClick={this.chengeEditMode} type="button" className="btn btn-secondary">Edit</button>
            </span>
    }

    render() {
        return this.state.isInEditMode ? this.renderEditView() : this.renderDefaultView();
    }
}

export default EditableTitle