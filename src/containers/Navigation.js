import React, { Component } from 'react';

class Navigation extends Component {
    render() {
        return (
            <ul className="nav justify-content-center">
                <li className="nav-item">
                    <a className="nav-link" href="/#/extended-articles/">Extended Articles</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="/#/base-articles/">Base Articles</a>
                </li>
            </ul>
        );
    }
}

export default Navigation;